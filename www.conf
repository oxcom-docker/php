[global]
error_log = /proc/self/fd/2
log_limit = 8192

; Start a new pool named 'www'.
; the variable $pool can be used in any directive and will be replaced by the
; pool name ('www' here)
[www]

; Unix user/group of processes
; Note: The user is mandatory. If the group is not set, the default user's group
;       will be used.
user = www-data
group = www-data

; The address on which to accept FastCGI requests.
; Valid syntaxes are:
;   'ip.add.re.ss:port'    - to listen on a TCP socket to a specific IPv4 address on
;                            a specific port;
;   '[ip:6:addr:ess]:port' - to listen on a TCP socket to a specific IPv6 address on
;                            a specific port;
;   'port'                 - to listen on a TCP socket to all addresses
;                            (IPv6 and IPv4-mapped) on a specific port;
;   '/path/to/unix/socket' - to listen on a unix socket.
; Note: This value is mandatory.
listen = 9000

; Set permissions for unix socket, if one is used. In Linux, read/write
; permissions must be set in order to allow connections from a web server. Many
; BSD-derived systems allow connections regardless of permissions.
; Default Values: user and group are set as the running user
;                 mode is set to 0660
listen.owner = www-data
listen.group = www-data
;listen.mode = 0660
; When POSIX Access Control Lists are supported you can set them using
; these options, value is a comma separated list of user/group names.
; When set, listen.owner and listen.group are ignored
;listen.acl_users =
;listen.acl_groups =

; Choose how the process manager will control the number of child processes.
pm = dynamic

; The number of child processes to be created when pm is set to 'static' and the
; maximum number of child processes when pm is set to 'dynamic' or 'ondemand'.
pm.max_children = 800

; The number of child processes created on startup.
pm.start_servers = 5

; The desired minimum number of idle server processes.
pm.min_spare_servers = 5

; The desired maximum number of idle server processes.
pm.max_spare_servers = 35

php_flag[display_errors] = off

; Allow docker to track php-fpm logs
php_admin_value[error_log] = /proc/self/fd/2
php_admin_flag[log_errors] = on

catch_workers_output = yes
decorate_workers_output = no
clear_env = on

; Tune memory
php_admin_value[memory_limit] = 512M

; XDebug config
; Allow remote hosts to enable debugging, in this case connecting back to the automatically maintained DNS
; name referencing our workstation (it should be provided by --add-host option): host.docker.internal
;php_admin_flag[xdebug.remote_enable] = on
;php_admin_flag[xdebug.remote_autostart] = on
;php_admin_value[xdebug.remote_host] = host.docker.internal
;php_admin_value[xdebug.var_display_max_depth] = 256
;php_admin_value[xdebug.var_display_max_children] = 256
;php_admin_value[xdebug.var_display_max_data] = 256

; Performance configuration
;php_admin_flag[opcache.enable] = on
;php_admin_flag[opcache.use_cwd] = on
;php_admin_flag[opcache.validate_timestamps] = off
;php_admin_flag[opcache.revalidate_path] = off
;php_admin_flag[opcache.enable_file_override] = on
;php_admin_flag[opcache.huge_code_pages] = on

;php_admin_value[opcache.max_wasted_percentage] = 5
;php_admin_value[opcache.memory_consumption] = 128
;php_admin_value[opcache.interned_strings_buffer] = 16
;php_admin_value[opcache.max_file_size] = 0

; Symfony support
;php_admin_flag[opcache.save_comments] = on
;php_admin_flag[opcache.load_comments] = on
; Magento support
;php_admin_value[opcache.max_accelerated_files] = 11000
