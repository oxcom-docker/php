# PHP Docker images

Source for the PHP-FPM [docker images](https://hub.docker.com/repository/docker/oxcom/php).

List of installed (in additional to default official images) extensions:
- amqp
- intl
- curl
- json
- mbstring
- mysql
- apcu
- redis
- bcmath
- sqlite3
- xml
- sockets
- imagick (with ```webp``` support)
- xdebug (only in ```-dev``` images)

## Local build
Alpine:
```bash
docker build . --rm -t php-7.3-alpine:latest -f ./7.3/alpine/Dockerfile
docker build . --rm -t php-7.4-alpine:latest -f ./7.4/alpine/Dockerfile
docker build . --rm -t php-8.0-alpine:latest -f ./8.0/alpine/Dockerfile
docker build . --rm -t php-8.1-alpine:latest -f ./8.1/alpine/Dockerfile
```

Debian:
```bash
docker build . --rm -t php-7.3-debian:latest -f ./7.3/debian/Dockerfile
docker build . --rm -t php-7.4-debian:latest -f ./7.4/debian/Dockerfile
docker build . --rm -t php-8.0-debian:latest -f ./8.0/debian/Dockerfile
docker build . --rm -t php-8.1-debian:latest -f ./8.1/debian/Dockerfile
```

Use ```--build-arg SANDBOX=yes``` to enable ```xdebug```


## PHP Imagick and Webp
1. PHP Imagick extension too slow when working with native support of webp. So there is a nice hack to speedup the process.

    ```bash
    find /usr/lib/ -type f -name webp.la -exec rm -rf {} \;
    find /usr/lib/ -type f -name webp.so -exec rm -rf {} \;
    ```

2. PHP Imagick produce low quality of webp images, because does not apply any parameters for delegated call:
    ```bashgpbn
    $ convert -list delegate | grep webp
        png  <= webp  "cwebp' -quiet %Q '%i' -o '%o"
        webp =>       "dwebp' -pam '%i' -o '%o"
    ```
   The fix can be done in 
    ```bash
    $ find /etc/ -type f -name delegates.xml
    /etc/ImageMagick-6/delegates.xml
    ```
   
   Apply diff for ```delegates.xml``` configuration
   ```diff
   - <delegate decode="png" encode="webp" command="&quot;cwebp&quot; -quiet %Q &quot;%i&quot; -o &quot;%o&quot;"/>
   + <delegate decode="png" encode="webp" command="&quot;cwebp&quot; -quiet -q %Q -mt -m 6 -z 9 -alpha_filter best -psnr 52 -pass 10 &quot;%i&quot; -o &quot;%o&quot;"/>
   ```   

## TODO
Cover with tests:

- installed extensions are there and functional
- we can run php code related for extension
- image has been converted png => jpg => webp
