image: docker:stable

services:
    - docker:dind

stages:
    - pre-build
    - build
    - test
    - release

variables:
    DOCKERHUB_ACCOUNT: oxcom
    DOCKERHUB_REGISTRY: docker.io
    DOCKERHUB_IMAGE: $DOCKERHUB_REGISTRY/$DOCKERHUB_ACCOUNT/php

    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
    # https://gitlab.com/gitlab-org/gitlab-ce/issues/64959
    DOCKER_TLS_CERTDIR: ""

before_script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY

.pre_build_template: &pre_build_template
    stage: pre-build
    script: docker run --rm -v `pwd`:/mnt/target hadolint/hadolint /bin/hadolint --ignore DL3008 --ignore DL3018 --ignore SC2086 /mnt/target/$PHP_VERSION/$BUILD_OS/Dockerfile

.build_template: &build_template
    stage: build
    script:
        - docker pull $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS:latest || true
        - docker build . --rm --tag $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS:latest --tag $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS:$CI_COMMIT_SHORT_SHA -f ./$PHP_VERSION/$BUILD_OS/Dockerfile --cache-from $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS:latest
        - docker push $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS:latest
        - docker push $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS:$CI_COMMIT_SHORT_SHA

.build_template_dev: &build_template_dev
    stage: build
    script:
        - docker pull $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS-dev:latest || true
        - docker build . --rm --tag $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS-dev:latest --tag $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS-dev:$CI_COMMIT_SHORT_SHA -f ./$PHP_VERSION/$BUILD_OS/Dockerfile --cache-from $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS-dev:latest --build-arg SANDBOX=yes
        - docker push $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS-dev:latest
        - docker push $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS-dev:$CI_COMMIT_SHORT_SHA

.test_template: &test_template
    stage: test
    script:
        - docker run --rm $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS:latest php --version
        - docker run --rm $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS:latest composer --version
        - docker run --rm $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS:latest php -m | grep bcmath
        - docker run --rm $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS:latest php -m | grep curl
        - docker run --rm $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS:latest php -m | grep intl
        - docker run --rm $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS:latest php -m | grep imagick
        - docker run --rm $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS:latest php -m | grep mbstring
        - docker run --rm $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS:latest php -m | grep pdo_mysql
        - docker run --rm $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS:latest php -m | grep redis
        - docker run --rm $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS:latest php -m | grep zlib
        - docker run --rm $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS:latest php -m | grep amqp

.test_template_dev: &test_template_dev
    extends: .test_template
    script:
        - docker run --rm $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS-dev:latest php -m | grep xdebug


.release_template: &release_template
    stage: release
    script:
        - echo "$DOCKERHUB_TOKEN" | docker login -u $DOCKERHUB_ACCOUNT --password-stdin $DOCKERHUB_REGISTRY
        - docker pull $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS:latest
        - docker tag $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS:latest $DOCKERHUB_IMAGE:$PHP_VERSION-$BUILD_OS
        - docker push $DOCKERHUB_IMAGE:$PHP_VERSION-$BUILD_OS

.release_template_dev: &release_template_dev
    stage: release
    script:
        - echo "$DOCKERHUB_TOKEN" | docker login -u $DOCKERHUB_ACCOUNT --password-stdin $DOCKERHUB_REGISTRY
        - docker pull $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS-dev:latest
        - docker tag $CI_REGISTRY_IMAGE/$PHP_VERSION-$BUILD_OS-dev:latest $DOCKERHUB_IMAGE:$PHP_VERSION-$BUILD_OS-dev
        - docker push $DOCKERHUB_IMAGE:$PHP_VERSION-$BUILD_OS-dev

# Alpine pre-builds
pre-build:73-alpine:
    <<: *pre_build_template
    variables:
        PHP_VERSION: '7.3'
        BUILD_OS: alpine

pre-build:74-alpine:
    <<: *pre_build_template
    variables:
        PHP_VERSION: '7.4'
        BUILD_OS: alpine

pre-build:80-alpine:
    <<: *pre_build_template
    variables:
        PHP_VERSION: '8.0'
        BUILD_OS: alpine

pre-build:81-alpine:
    <<: *pre_build_template
    variables:
        PHP_VERSION: '8.1'
        BUILD_OS: alpine

pre-build:83-alpine:
    <<: *pre_build_template
    variables:
        PHP_VERSION: '8.3'
        BUILD_OS: alpine

# Alpine builds
build:73-alpine:
    <<: *build_template
    variables:
        PHP_VERSION: '7.3'
        BUILD_OS: alpine
    needs:
        - pre-build:73-alpine

build:74-alpine:
    <<: *build_template
    variables:
        PHP_VERSION: '7.4'
        BUILD_OS: alpine
    needs:
        - pre-build:74-alpine

build:80-alpine:
    <<: *build_template
    variables:
        PHP_VERSION: '8.0'
        BUILD_OS: alpine
    needs:
        - pre-build:80-alpine

build:81-alpine:
    <<: *build_template
    variables:
        PHP_VERSION: '8.1'
        BUILD_OS: alpine
    needs:
        - pre-build:81-alpine

build:83-alpine:
    <<: *build_template
    variables:
        PHP_VERSION: '8.3'
        BUILD_OS: alpine
    needs:
        - pre-build:83-alpine

build:73-alpine-dev:
    <<: *build_template_dev
    variables:
        PHP_VERSION: '7.3'
        BUILD_OS: alpine
    needs:
        - pre-build:73-alpine

build:74-alpine-dev:
    <<: *build_template_dev
    variables:
        PHP_VERSION: '7.4'
        BUILD_OS: alpine
    needs:
        - pre-build:74-alpine

build:80-alpine-dev:
    <<: *build_template_dev
    variables:
        PHP_VERSION: '8.0'
        BUILD_OS: alpine
    needs:
        - pre-build:80-alpine

build:81-alpine-dev:
    <<: *build_template_dev
    variables:
        PHP_VERSION: '8.1'
        BUILD_OS: alpine
    needs:
        - pre-build:81-alpine

build:83-alpine-dev:
    <<: *build_template_dev
    variables:
        PHP_VERSION: '8.3'
        BUILD_OS: alpine
    needs:
        - pre-build:83-alpine

# Check Alpine builds
test:73-alpine:
    <<: *test_template
    variables:
        PHP_VERSION: '7.3'
        BUILD_OS: alpine
    needs:
        - build:73-alpine

test:74-alpine:
    <<: *test_template
    variables:
        PHP_VERSION: '7.4'
        BUILD_OS: alpine
    needs:
        - build:74-alpine

test:80-alpine:
    <<: *test_template
    variables:
        PHP_VERSION: '8.0'
        BUILD_OS: alpine
    needs:
        - build:80-alpine

test:81-alpine:
    <<: *test_template
    variables:
        PHP_VERSION: '8.1'
        BUILD_OS: alpine
    needs:
        - build:81-alpine

test:83-alpine:
    <<: *test_template
    variables:
        PHP_VERSION: '8.3'
        BUILD_OS: alpine
    needs:
        - build:83-alpine

test:73-alpine-dev:
    <<: *test_template_dev
    variables:
        PHP_VERSION: '7.3'
        BUILD_OS: alpine
    needs:
        - build:73-alpine-dev

test:74-alpine-dev:
    <<: *test_template_dev
    variables:
        PHP_VERSION: '7.4'
        BUILD_OS: alpine
    needs:
        - build:74-alpine-dev

test:80-alpine-dev:
    <<: *test_template_dev
    variables:
        PHP_VERSION: '8.0'
        BUILD_OS: alpine
    needs:
        - build:80-alpine-dev

test:81-alpine-dev:
    <<: *test_template_dev
    variables:
        PHP_VERSION: '8.1'
        BUILD_OS: alpine
    needs:
        - build:81-alpine-dev

test:83-alpine-dev:
    <<: *test_template_dev
    variables:
        PHP_VERSION: '8.3'
        BUILD_OS: alpine
    needs:
        - build:83-alpine-dev

# Release alpine builds
release:73-alpine:
    <<: *release_template
    variables:
        PHP_VERSION: '7.3'
        BUILD_OS: alpine
    needs:
        - test:73-alpine

release:74-alpine:
    <<: *release_template
    variables:
        PHP_VERSION: '7.4'
        BUILD_OS: alpine
    needs:
        - test:74-alpine

release:80-alpine:
    <<: *release_template
    variables:
        PHP_VERSION: '8.0'
        BUILD_OS: alpine
    needs:
        - test:80-alpine

release:81-alpine:
    <<: *release_template
    variables:
        PHP_VERSION: '8.1'
        BUILD_OS: alpine
    needs:
        - test:81-alpine

release:83-alpine:
    <<: *release_template
    variables:
        PHP_VERSION: '8.3'
        BUILD_OS: alpine
    needs:
        - test:83-alpine

release:73-alpine-dev:
    <<: *release_template_dev
    variables:
        PHP_VERSION: '7.3'
        BUILD_OS: alpine
    needs:
        - test:73-alpine-dev

release:74-alpine-dev:
    <<: *release_template_dev
    variables:
        PHP_VERSION: '7.4'
        BUILD_OS: alpine
    needs:
        - test:74-alpine-dev

release:80-alpine-dev:
    <<: *release_template_dev
    variables:
        PHP_VERSION: '8.0'
        BUILD_OS: alpine
    needs:
        - test:80-alpine-dev

release:81-alpine-dev:
    <<: *release_template_dev
    variables:
        PHP_VERSION: '8.1'
        BUILD_OS: alpine
    needs:
        - test:81-alpine-dev

release:83-alpine-dev:
    <<: *release_template_dev
    variables:
        PHP_VERSION: '8.3'
        BUILD_OS: alpine
    needs:
        - test:83-alpine-dev

# Debian pre-builds
pre-build:73-debian:
    <<: *pre_build_template
    variables:
        PHP_VERSION: '7.3'
        BUILD_OS: debian

pre-build:74-debian:
    <<: *pre_build_template
    variables:
        PHP_VERSION: '7.4'
        BUILD_OS: debian

pre-build:80-debian:
    <<: *pre_build_template
    variables:
        PHP_VERSION: '8.0'
        BUILD_OS: debian

pre-build:81-debian:
    <<: *pre_build_template
    variables:
        PHP_VERSION: '8.1'
        BUILD_OS: debian

pre-build:83-debian:
    <<: *pre_build_template
    variables:
        PHP_VERSION: '8.3'
        BUILD_OS: debian

# Debian builds
build:73-debian:
    <<: *build_template
    variables:
        PHP_VERSION: '7.3'
        BUILD_OS: debian
    needs:
        - pre-build:73-debian

build:74-debian:
    <<: *build_template
    variables:
        PHP_VERSION: '7.4'
        BUILD_OS: debian
    needs:
        - pre-build:74-debian

build:80-debian:
    <<: *build_template
    variables:
        PHP_VERSION: '8.0'
        BUILD_OS: debian
    needs:
        - pre-build:80-debian

build:81-debian:
    <<: *build_template
    variables:
        PHP_VERSION: '8.1'
        BUILD_OS: debian
    needs:
        - pre-build:81-debian

build:83-debian:
    <<: *build_template
    variables:
        PHP_VERSION: '8.3'
        BUILD_OS: debian
    needs:
        - pre-build:83-debian

build:73-debian-dev:
    <<: *build_template_dev
    variables:
        PHP_VERSION: '7.3'
        BUILD_OS: debian
    needs:
        - pre-build:73-debian

build:74-debian-dev:
    <<: *build_template_dev
    variables:
        PHP_VERSION: '7.4'
        BUILD_OS: debian
    needs:
        - pre-build:74-debian

build:80-debian-dev:
    <<: *build_template_dev
    variables:
        PHP_VERSION: '8.0'
        BUILD_OS: debian
    needs:
        - pre-build:80-debian

build:81-debian-dev:
    <<: *build_template_dev
    variables:
        PHP_VERSION: '8.1'
        BUILD_OS: debian
    needs:
        - pre-build:81-debian

build:83-debian-dev:
    <<: *build_template_dev
    variables:
        PHP_VERSION: '8.3'
        BUILD_OS: debian
    needs:
        - pre-build:83-debian

# Check Debian builds
test:73-debian:
    <<: *test_template
    variables:
        PHP_VERSION: '7.3'
        BUILD_OS: debian
    needs:
        - build:73-debian

test:74-debian:
    <<: *test_template
    variables:
        PHP_VERSION: '7.4'
        BUILD_OS: debian
    needs:
        - build:74-debian

test:80-debian:
    <<: *test_template
    variables:
        PHP_VERSION: '8.0'
        BUILD_OS: debian
    needs:
        - build:80-debian

test:81-debian:
    <<: *test_template
    variables:
        PHP_VERSION: '8.1'
        BUILD_OS: debian
    needs:
        - build:81-debian

test:83-debian:
    <<: *test_template
    variables:
        PHP_VERSION: '8.3'
        BUILD_OS: debian
    needs:
        - build:83-debian

test:73-debian-dev:
    <<: *test_template_dev
    variables:
        PHP_VERSION: '7.3'
        BUILD_OS: debian
    needs:
        - build:73-debian-dev

test:74-debian-dev:
    <<: *test_template_dev
    variables:
        PHP_VERSION: '7.4'
        BUILD_OS: debian
    needs:
        - build:74-debian-dev

test:80-debian-dev:
    <<: *test_template_dev
    variables:
        PHP_VERSION: '8.0'
        BUILD_OS: debian
    needs:
        - build:80-debian-dev

test:81-debian-dev:
    <<: *test_template_dev
    variables:
        PHP_VERSION: '8.1'
        BUILD_OS: debian
    needs:
        - build:81-debian-dev

test:83-debian-dev:
    <<: *test_template_dev
    variables:
        PHP_VERSION: '8.3'
        BUILD_OS: debian
    needs:
        - build:83-debian-dev

# Release debian builds
release:73-debian:
    <<: *release_template
    variables:
        PHP_VERSION: '7.3'
        BUILD_OS: debian
    needs:
        - test:73-debian

release:74-debian:
    <<: *release_template
    variables:
        PHP_VERSION: '7.4'
        BUILD_OS: debian
    needs:
        - test:74-debian

release:80-debian:
    <<: *release_template
    variables:
        PHP_VERSION: '8.0'
        BUILD_OS: debian
    needs:
        - test:80-debian

release:81-debian:
    <<: *release_template
    variables:
        PHP_VERSION: '8.1'
        BUILD_OS: debian
    needs:
        - test:81-debian

release:83-debian:
    <<: *release_template
    variables:
        PHP_VERSION: '8.3'
        BUILD_OS: debian
    needs:
        - test:83-debian

release:73-debian-dev:
    <<: *release_template_dev
    variables:
        PHP_VERSION: '7.3'
        BUILD_OS: debian
    needs:
        - test:73-debian-dev

release:74-debian-dev:
    <<: *release_template_dev
    variables:
        PHP_VERSION: '7.4'
        BUILD_OS: debian
    needs:
        - test:74-debian-dev

release:80-debian-dev:
    <<: *release_template_dev
    variables:
        PHP_VERSION: '8.0'
        BUILD_OS: debian
    needs:
        - test:80-debian-dev

release:81-debian-dev:
    <<: *release_template_dev
    variables:
        PHP_VERSION: '8.1'
        BUILD_OS: debian
    needs:
        - test:81-debian-dev

release:83-debian-dev:
    <<: *release_template_dev
    variables:
        PHP_VERSION: '8.3'
        BUILD_OS: debian
    needs:
        - test:83-debian-dev
