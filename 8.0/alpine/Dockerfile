FROM php:8.0-fpm-alpine

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

ENV TERM=xterm \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8 \
    LC_CTYPE=en_US.UTF-8 \
    LC_ALL=en_US.UTF-8

ARG SANDBOX=no

# PHP-FPM configuration
COPY www.conf /tmp/www.conf

# persistent deps: curl, ca-certificates, tar, xz, libressl
RUN apk add --update --no-cache icu-libs linux-headers \
    && apk update \
    && apk add --no-cache --virtual .build-dependencies \
        $PHPIZE_DEPS \
        socat \
        tzdata \
        findutils \
        libzip-dev \
        libzip \
        zip \
        libxml2-dev \
        libtool \
        libcurl \
        libssh2 \
        libssh2-dev \
        libintl \
        icu-libs \
        icu-dev \
        icu \
        libxslt-dev \
        imagemagick-dev \
        rabbitmq-c-dev \
        oniguruma-dev \
    # set timezone
    && ln -fs /usr/share/zoneinfo/GMT /etc/localtime \
    # Pre configuration
    && docker-php-ext-configure bcmath --enable-bcmath \
    && docker-php-ext-configure zip \
    && docker-php-ext-configure intl \
    && docker-php-ext-configure mbstring --enable-mbstring \
    # Were installed:
    # curl, libsodium, mhash, mbstring, mysqlnd, openssl, zlib, json, xml
    && pecl install -o -f apcu \
    && docker-php-ext-enable apcu \
    && docker-php-ext-install pdo pdo_mysql mysqli \
    && docker-php-ext-install sockets \
    && pecl install -o -f redis \
    # 21.06.2021 there is no stable build for AMQP with PHP8 support
    # && pecl install -o -f amqp \
    && pecl install -o -f amqp-1.11.0beta \
    && docker-php-ext-enable redis \
    && docker-php-ext-enable amqp \
    && docker-php-ext-install bcmath \
    && docker-php-ext-install zip \
    && docker-php-ext-install intl \
    # ImageMagic support
    && pecl install -o -f imagick \
    && docker-php-ext-enable --ini-name 20-imagick.ini imagick \
    # clean up
    && apk del .build-dependencies \
    && apk add --no-cache libzip imagemagick imagemagick-libs libwebp libwebp-tools rabbitmq-c oniguruma libgomp \
    && mkdir -p /var/run/php/ \
    && mkdir -p /var/www/app/ \
    && { \
    		echo '[global]'; \
    		echo 'daemonize = no'; \
    		echo; \
    	} | tee /usr/local/etc/php-fpm.d/zz-docker.conf \
    && curl -s https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer \
    && composer self-update \
    && if [ "$SANDBOX" = "yes" ] ; then \
        apk add --no-cache $PHPIZE_DEPS \
        && pecl install xdebug \
        && docker-php-ext-enable xdebug \
        && apk del $PHPIZE_DEPS \
        && sed -Eri 's/;php_admin_(flag|value)\[xdebug/\php_admin_\1[xdebug/g' /tmp/www.conf; \
    else \
        sed -Eri 's/;php_admin_(flag|value)\[opcache/\php_admin_\1[opcache/g' /tmp/www.conf; \
    fi \
    && mv /tmp/www.conf /usr/local/etc/php-fpm.d/www.conf \
    && docker-php-source delete \
    && rm -rf /tmp/* /var/cache/apk/*


EXPOSE 9000
WORKDIR /var/www/app/
